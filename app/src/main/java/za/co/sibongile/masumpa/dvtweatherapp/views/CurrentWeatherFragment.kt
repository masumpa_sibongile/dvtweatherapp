package za.co.sibongile.masumpa.dvtweatherapp.views

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import za.co.sibongile.masumpa.dvtweatherapp.R
import za.co.sibongile.masumpa.dvtweatherapp.databinding.ViewCurrentForecastBinding
import za.co.sibongile.masumpa.dvtweatherapp.interfaces.FailureListener
import za.co.sibongile.masumpa.dvtweatherapp.utils.DialogUtil
import za.co.sibongile.masumpa.dvtweatherapp.viewmodel.CurrentWeatherViewModel
import za.co.sibongile.masumpa.dvtweatherapp.viewmodel.CurrentWeatherViewModelFactory

@SuppressLint("ValidFragment")
class CurrentWeatherFragment @SuppressLint("ValidFragment") constructor() : Fragment(), KodeinAware{

    override val kodein by kodein()
    private val viewModelFactory: CurrentWeatherViewModelFactory by this.instance()

    private lateinit var binding: ViewCurrentForecastBinding
    private lateinit var viewModel: CurrentWeatherViewModel

    var failureListener: FailureListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            failureListener = context as FailureListener
        } catch (e: Exception) {
            e.stackTrace
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.view_current_forecast, container, false)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CurrentWeatherViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.location.observe(viewLifecycleOwner, Observer {
            it.let {
                viewModel.loadWeather(it.latitude,it.longitude)
            }
        })

        return binding.root
    }

    fun setLocation(recentLocation: Location){
        viewModel.location.value = recentLocation
    }
}