package za.co.sibongile.masumpa.dvtweatherapp.utils

import android.content.Context
import com.google.android.gms.common.GoogleApiAvailability
import com.huawei.hms.api.HuaweiApiAvailability

object HmsApiCheckUtil {
    fun isHmsApiPreferred(context: Context): Boolean {
        return isHmsAvailable(context) && !isGmsAvailable(context)
    }

    private fun isHmsAvailable(context: Context): Boolean {
        val result: Int =
            HuaweiApiAvailability.getInstance().isHuaweiMobileServicesAvailable(context)
        return com.huawei.hms.api.ConnectionResult.SUCCESS === result
    }

    fun isGmsAvailable(context: Context?): Boolean {
        val result: Int = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context)
        return com.google.android.gms.common.ConnectionResult.SUCCESS === result
    }
}