package za.co.sibongile.masumpa.dvtweatherapp.views

import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import za.co.sibongile.masumpa.dvtweatherapp.R
import za.co.sibongile.masumpa.dvtweatherapp.databinding.ViewForecastBinding
import za.co.sibongile.masumpa.dvtweatherapp.interfaces.FailureListener
import za.co.sibongile.masumpa.dvtweatherapp.utils.DialogUtil
import za.co.sibongile.masumpa.dvtweatherapp.viewmodel.ForecastWeatherViewModel
import za.co.sibongile.masumpa.dvtweatherapp.viewmodel.ForecastWeatherViewModelFactory


@SuppressLint("ValidFragment")
class ForecastWeatherFragment @SuppressLint("ValidFragment") constructor() : Fragment(), KodeinAware,
    FailureListener {

    override val kodein by kodein()
    private val viewModelFactory: ForecastWeatherViewModelFactory by this.instance()

    private lateinit var binding: ViewForecastBinding
    lateinit var viewModel: ForecastWeatherViewModel


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.view_forecast, container, false)

        binding.forcastList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(ForecastWeatherViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.location.observe(viewLifecycleOwner, Observer {
            it.let {
                viewModel.loadWeather(it.latitude,it.longitude)
            }
        })

        return binding.root
    }

    fun setLocation(recentLocation: Location){
        viewModel.location.value = recentLocation
    }

    override fun onFailure(message: String) {
        DialogUtil.showAlertDialog(requireActivity(), message)
    }
}