package za.co.sibongile.masumpa.dvtweatherapp.data.network

import kotlinx.coroutines.*

object Coroutines {
    fun main(work: suspend (() -> Unit)) =
        CoroutineScope(Dispatchers.Main).launch {
            work()
        }

    fun <T : Any> ioThenMain(
        handler: CoroutineExceptionHandler,
        work: suspend (() -> T?),
        callback: ((T?) -> Unit)
    ) =
        CoroutineScope(Dispatchers.Main).launch(handler) {

            val data = CoroutineScope(Dispatchers.IO).async rt@{
                return@rt work()
            }.await()
            callback(data)
        }

}