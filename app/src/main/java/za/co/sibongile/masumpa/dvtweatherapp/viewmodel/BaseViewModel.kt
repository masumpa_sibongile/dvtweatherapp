package za.co.sibongile.masumpa.dvtweatherapp.viewmodel

import android.location.Location
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Job
import za.co.sibongile.masumpa.dvtweatherapp.interfaces.FailureListener

abstract class BaseViewModel: ViewModel(){

    lateinit var job: Job

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    var location: MutableLiveData<Location> = MutableLiveData()

    var progressListener: FailureListener? = null

    abstract fun loadWeather(latitude: Double, longitude: Double)

    fun onLoadingStart() {
        loadingVisibility.value = View.VISIBLE
    }

    fun onLoadingStop() {
        loadingVisibility.value = View.GONE
    }

    val handler = CoroutineExceptionHandler { _, exception ->
        progressListener?.onFailure(exception.message!!)
    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }
}