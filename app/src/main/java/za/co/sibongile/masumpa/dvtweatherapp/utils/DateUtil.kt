package za.co.sibongile.masumpa.dvtweatherapp.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtil {
    @JvmStatic
    fun getTodayName(date: String): String {
        val format = SimpleDateFormat("EEEE", Locale.ENGLISH)
        return format.format(convertStringDate(date))
    }

    fun convertStringDate(date: String): Date {
        val formatter = SimpleDateFormat("yyyy-MM-yy HH:mm:ss", Locale.ENGLISH)
        return formatter.parse(date)
    }
}