package za.co.sibongile.masumpa.dvtweatherapp

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.google.android.gms.location.LocationServices
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import za.co.sibongile.masumpa.dvtweatherapp.data.network.ApiClient
import za.co.sibongile.masumpa.dvtweatherapp.data.network.NetworkConnectionInterceptor
import za.co.sibongile.masumpa.dvtweatherapp.data.network.repository.ForecastRepository
import za.co.sibongile.masumpa.dvtweatherapp.viewmodel.CurrentWeatherViewModelFactory
import za.co.sibongile.masumpa.dvtweatherapp.viewmodel.ForecastWeatherViewModelFactory

class DVTWeatherApplication : MultiDexApplication(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@DVTWeatherApplication))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { ApiClient(instance()) }
        bind() from singleton { ForecastRepository(instance()) }
        bind() from provider { CurrentWeatherViewModelFactory(instance()) }
        bind() from provider { ForecastWeatherViewModelFactory(instance()) }
        bind() from provider { LocationServices.getFusedLocationProviderClient(instance<Context>()) }
    }

}