package za.co.sibongile.masumpa.dvtweatherapp

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import za.co.sibongile.masumpa.dvtweatherapp.interfaces.iLoadFragment
import za.co.sibongile.masumpa.dvtweatherapp.managers.GLifecycleBoundLocationManager
import za.co.sibongile.masumpa.dvtweatherapp.utils.DialogUtil
import za.co.sibongile.masumpa.dvtweatherapp.utils.HmsApiCheckUtil
import za.co.sibongile.masumpa.dvtweatherapp.utils.ViewUtil
import za.co.sibongile.masumpa.dvtweatherapp.views.CurrentWeatherFragment
import za.co.sibongile.masumpa.dvtweatherapp.views.ForecastWeatherFragment

class MainActivity : AppCompatActivity(), KodeinAware, iLoadFragment {

    override val kodein by kodein()
    private val gFusedLocationProviderClient: FusedLocationProviderClient by this.instance()

    private val gLocationCallback = object : LocationCallback() {
        override fun onLocationResult(location: LocationResult?) {
            super.onLocationResult(location)
            location?.lastLocation?.let {
                currentForecastView?.setLocation(it)
                forecastView?.setLocation(it)
            }
        }
    }

    // Fragments
    private var currentForecastView: CurrentWeatherFragment? = null
    private var forecastView: ForecastWeatherFragment? = null

    private val PERMISSION_REQUEST_CODE = 2020

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_main)

        if (HmsApiCheckUtil.isHmsApiPreferred(this)) {
            // TODO request access using HUAWEI
        } else {
            requestPermission()
        }

        loadCurrentForcastView()
        loadForecastView()
    }

    override fun loadCurrentForcastView() {
        currentForecastView = CurrentWeatherFragment()
        ViewUtil.addFragmentToActivity(supportFragmentManager, currentForecastView!!, R.id.top_view)
    }

    override fun loadForecastView() {
        forecastView = ForecastWeatherFragment()
        ViewUtil.addFragmentToActivity(supportFragmentManager, forecastView!!, R.id.bottom_view)
    }


    private fun requestPermission() {
        if (ActivityCompat.checkSelfPermission(
                this,
                ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION),
                    PERMISSION_REQUEST_CODE
                )
            }
            return
        }

        bindLocationManager()
    }

    private fun bindLocationManager() {
        if (HmsApiCheckUtil.isHmsApiPreferred(this)) {
            //TODO implement HmsLifecycleBoundLocationManager
        } else {
            GLifecycleBoundLocationManager(
                this,
                gFusedLocationProviderClient, gLocationCallback
            )
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    bindLocationManager()
                } else {
                    DialogUtil.showAlertDialog(this,
                        getString(R.string.access_to_location_message),
                        getString(
                            R.string.text_ok
                        ),
                        DialogInterface.OnClickListener { dialogInterface, i ->
                            requestPermission()
                        })
                }
            }
        }
    }
}
