package za.co.sibongile.masumpa.dvtweatherapp.interfaces

interface FailureListener{
    fun onFailure(message: String)
}