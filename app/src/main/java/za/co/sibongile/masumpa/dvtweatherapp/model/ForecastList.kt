package za.co.sibongile.masumpa.dvtweatherapp.model

import com.google.gson.annotations.SerializedName
import za.co.sibongile.masumpa.dvtweatherapp.utils.DateUtil

data class ForecastList(
    @SerializedName("dt_txt")
    var dt_txt : String,
    @SerializedName("main")
    val main: Main,
    @SerializedName("weather")
    val weather: List<Weather>){
    val _dt_txt: String
        get() = DateUtil.getTodayName(dt_txt)
}