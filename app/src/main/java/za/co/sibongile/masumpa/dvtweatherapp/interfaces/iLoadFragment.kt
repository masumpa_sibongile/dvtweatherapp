package za.co.sibongile.masumpa.dvtweatherapp.interfaces

interface iLoadFragment {
    fun loadCurrentForcastView()
    fun loadForecastView()
}