package za.co.sibongile.masumpa.dvtweatherapp.utils

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.text.Html
import android.view.View
import androidx.appcompat.app.AlertDialog

class DialogUtil {
    companion object {
        fun showAlertDialog(context: Context, message: String) {
            val builder = AlertDialog.Builder(context)
            builder.setMessage(Html.fromHtml("<font color='#000000'>$message</font>"))

            builder.setPositiveButton(android.R.string.ok) { dialog, which ->
                dialog.dismiss()
            }

            builder.show()

        }

        fun showAlertDialog(context: Context, message: String, btnName: String, onClickListener: DialogInterface.OnClickListener) {
            val builder = AlertDialog.Builder(context)
            builder.setMessage(Html.fromHtml("<font color='#000000'>$message</font>"))
            builder.setPositiveButton(Html.fromHtml("<font color='#000000'>$btnName</font>"), onClickListener)

            builder.show()

        }
    }
}