package za.co.sibongile.masumpa.dvtweatherapp.data.network.repository

import za.co.sibongile.masumpa.dvtweatherapp.data.network.ApiClient
import za.co.sibongile.masumpa.dvtweatherapp.data.network.SafeGuardApiRequest
import za.co.sibongile.masumpa.dvtweatherapp.data.network.response.CurrentResponse
import za.co.sibongile.masumpa.dvtweatherapp.data.network.response.ForecastResponse

class ForecastRepository(private val api: ApiClient) : SafeGuardApiRequest() {
    suspend fun doLoadCurrentWeather(
        latitude: Double,
        longitude: Double,
        appId: String
    ): CurrentResponse {
        return apiRequest { api.invoke().doLoadCurrentWeather(latitude, longitude, appId) }
    }

    suspend fun doLoadForecastWeather(
        latitude: Double,
        longitude: Double,
        appId: String
    ): ForecastResponse {
        return apiRequest { api.invoke().doLoadForecastWeather(latitude, longitude, appId) }
    }
}