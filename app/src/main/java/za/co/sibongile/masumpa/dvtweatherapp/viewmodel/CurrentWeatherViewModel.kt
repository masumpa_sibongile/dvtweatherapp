package za.co.sibongile.masumpa.dvtweatherapp.viewmodel

import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.MutableLiveData
import za.co.sibongile.masumpa.dvtweatherapp.Constants.APP_ID
import za.co.sibongile.masumpa.dvtweatherapp.data.network.Coroutines
import za.co.sibongile.masumpa.dvtweatherapp.data.network.repository.ForecastRepository
import za.co.sibongile.masumpa.dvtweatherapp.data.network.response.CurrentResponse
import java.util.*
import kotlin.math.roundToInt

class CurrentWeatherViewModel(private val repository: ForecastRepository) : BaseViewModel() {

    private val currentDegrees = MutableLiveData<String>()
    private val weatherDescription = MutableLiveData<String>()
    private val weatherType = MutableLiveData<String>()
    private val minDegrees = MutableLiveData<String>()
    private val maxDegrees = MutableLiveData<String>()

    val displayCurrentWeatherView: MutableLiveData<Int> = MutableLiveData()

    init {
        displayCurrentWeatherView.value = GONE
    }

    override fun loadWeather(latitude: Double, longitude: Double) {
        onLoadingStart()
        job = Coroutines.ioThenMain(handler,
            {
                repository.doLoadCurrentWeather(latitude, longitude, APP_ID)
            },
            { currentResponse ->
                currentResponse.let {
                    bind(it!!)
                    onLoadingStop()
                }
            }
        )
    }

    private fun bind(current: CurrentResponse) {
        current.let {
            currentDegrees.value = it.main?.temp?.roundToInt().toString().dropLast(1)
            it.weather?.first().let { weatherItem ->
                weatherDescription.value = weatherItem?.description?.toUpperCase(Locale.ROOT)
                weatherType.value = weatherItem?.main
            }

            it.main.let { temperature ->
                minDegrees.value = temperature?.temp_min?.roundToInt().toString().dropLast(1)
                maxDegrees.value = temperature?.temp_max?.roundToInt().toString().dropLast(1)
            }

            displayCurrentWeatherView.value = VISIBLE
        }
    }

    fun getCurrentDegrees(): MutableLiveData<String> {
        return currentDegrees
    }

    fun getWeatherDescription(): MutableLiveData<String> {
        return weatherDescription
    }

    fun getWeatherType(): MutableLiveData<String> {
        return weatherType
    }

    fun getMinDegrees(): MutableLiveData<String> {
        return minDegrees
    }

    fun getMaxDegrees(): MutableLiveData<String> {
        return maxDegrees
    }
}