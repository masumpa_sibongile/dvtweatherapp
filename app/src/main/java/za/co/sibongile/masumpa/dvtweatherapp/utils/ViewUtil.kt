package za.co.sibongile.masumpa.dvtweatherapp.utils

import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.snackbar.Snackbar
import za.co.sibongile.masumpa.dvtweatherapp.R


object ViewUtil {
    @JvmStatic
    fun addFragmentToActivity(manager: FragmentManager, fragment: Fragment, frameId: Int) {

        val transaction = manager.beginTransaction()
        transaction.add(frameId, fragment)
        transaction.commit()

    }

    fun View.snackbar(message: String, onClickListener: View.OnClickListener) {
        Snackbar.make(
            this,
            message,
            Snackbar.LENGTH_INDEFINITE
        ).also { snackbar ->
            snackbar.setAction(resources.getString(R.string.text_retry)) {
                onClickListener.onClick(it)
            }
        }.show()
    }
}