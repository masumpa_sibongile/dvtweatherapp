package za.co.sibongile.masumpa.dvtweatherapp.viewmodel

import za.co.sibongile.masumpa.dvtweatherapp.Constants.APP_ID
import za.co.sibongile.masumpa.dvtweatherapp.adapters.ForecastAdapter
import za.co.sibongile.masumpa.dvtweatherapp.data.network.Coroutines
import za.co.sibongile.masumpa.dvtweatherapp.data.network.repository.ForecastRepository
import za.co.sibongile.masumpa.dvtweatherapp.model.ForecastList

class ForecastWeatherViewModel(private val repository: ForecastRepository) : BaseViewModel() {

    val forecastAdapter: ForecastAdapter = ForecastAdapter()

    override fun loadWeather(latitude: Double, longitude: Double) {
        onLoadingStart()
        job = Coroutines.ioThenMain(handler,
            {
                repository.doLoadForecastWeather(latitude, longitude, APP_ID)
            },
            { currentResponse ->
                currentResponse.let { forecastResponse ->
                    onLoadingStop()
                    bindList(forecastResponse?.list!!.distinctBy { it -> it._dt_txt })
                }
            }
        )
    }

    private fun bindList(forecastList: List<ForecastList>) {
        forecastAdapter.updateForecastList(forecastList)
    }
}