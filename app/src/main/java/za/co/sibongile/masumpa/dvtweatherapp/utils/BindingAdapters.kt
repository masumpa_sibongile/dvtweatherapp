package za.co.sibongile.masumpa.dvtweatherapp.utils


import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import za.co.sibongile.masumpa.dvtweatherapp.R
import za.co.sibongile.masumpa.dvtweatherapp.enums.WeatherType
import za.co.sibongile.masumpa.dvtweatherapp.utils.extension.getParentActivity


@BindingAdapter("mutableVisibility")
fun setMutableVisibility(view: View, visibility: MutableLiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && visibility != null) {
        visibility.observe(parentActivity, Observer { value -> view.visibility = value ?: View.VISIBLE })
    }
}

@BindingAdapter("displayCurrentWeatherView")
fun displayCurrentWeatherView(view: View, visibility: MutableLiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && visibility != null) {
        visibility.observe(parentActivity, Observer { value -> view.visibility = value ?: View.VISIBLE })
    }
}

@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
    val parentActivity:AppCompatActivity? = view.getParentActivity()
    if(parentActivity != null && text != null) {
        text.observe(parentActivity, Observer { value -> view.text = value?:""})
    }
}

@BindingAdapter("mutableImage")
fun setMutableImage(view: ImageView, type: MutableLiveData<String>?) {
    val parentActivity:AppCompatActivity? = view.getParentActivity()
    if(parentActivity != null && type != null) {
        type.observe(parentActivity, Observer { value ->
            when (value?.toUpperCase()){
                WeatherType.RAIN.toString() -> view.setImageResource(R.drawable.forest_rainy)
                WeatherType.CLOUDY.toString() -> view.setImageResource(R.drawable.forest_cloudy)
                else -> view.setImageResource(R.drawable.forest_sunny)
            } })
    }
}

@BindingAdapter("mutableIcon")
fun setMutableIcon(view: ImageView, type: MutableLiveData<String>?) {
    val parentActivity:AppCompatActivity? = view.getParentActivity()
    if(parentActivity != null && type != null) {
        type.observe(parentActivity, Observer { value ->
            when (value?.toUpperCase()){
                WeatherType.RAIN.toString() -> view.setImageResource(R.drawable.partlysunny)
                WeatherType.CLOUDY.toString() -> view.setImageResource(R.drawable.rain)
                else -> view.setImageResource(R.drawable.clear)
            } })
    }
}

@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    view.adapter = adapter
}
