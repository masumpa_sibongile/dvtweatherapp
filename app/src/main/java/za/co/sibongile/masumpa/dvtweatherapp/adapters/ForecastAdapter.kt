package za.co.sibongile.masumpa.dvtweatherapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import za.co.sibongile.masumpa.dvtweatherapp.R
import za.co.sibongile.masumpa.dvtweatherapp.databinding.ItemForecastBinding
import za.co.sibongile.masumpa.dvtweatherapp.model.ForecastList
import za.co.sibongile.masumpa.dvtweatherapp.viewmodel.ForecastItemViewModel

class ForecastAdapter : RecyclerView.Adapter<ForecastAdapter.ViewHolder>() {
    private lateinit var forcastList: List<ForecastList>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastAdapter.ViewHolder {
        val binding: ItemForecastBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_forecast, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ForecastAdapter.ViewHolder, position: Int) {
        holder.bind(forcastList[position])
    }

    override fun getItemCount(): Int {
        return if (::forcastList.isInitialized) forcastList.size else 0
    }

    fun updateForecastList(forecastList: List<ForecastList>) {
        this.forcastList = forecastList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemForecastBinding) : RecyclerView.ViewHolder(binding.root) {
        private val viewModel = ForecastItemViewModel()

        fun bind(forecast: ForecastList) {
            viewModel.bind(forecast)
            binding.viewModel = viewModel
        }
    }
}