package za.co.sibongile.masumpa.dvtweatherapp.model

import com.google.gson.annotations.SerializedName

data class Weather(
    @SerializedName("main")
    val main : String,
    @SerializedName("description")
    val description : String,
    @SerializedName("icon")
    var icon : String)