package za.co.sibongile.masumpa.dvtweatherapp.enums

enum class WeatherType {
    CLEAR, RAIN, CLOUDY
}