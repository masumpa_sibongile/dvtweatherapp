package za.co.sibongile.masumpa.dvtweatherapp.viewmodel

import androidx.lifecycle.MutableLiveData
import za.co.sibongile.masumpa.dvtweatherapp.data.network.repository.ForecastRepository
import za.co.sibongile.masumpa.dvtweatherapp.model.ForecastList
import kotlin.math.roundToInt

class ForecastItemViewModel() : BaseViewModel() {

    private val forecastDay = MutableLiveData<String>()
    private val weatherType = MutableLiveData<String>()
    private val forecastDegrees = MutableLiveData<String>()

    fun bind(forecast: ForecastList) {
        forecast.let{ item ->
            forecastDay.value = item._dt_txt
            weatherType.value = item.weather.first().main
            forecastDegrees.value = item.main.temp.roundToInt().toString().dropLast(1)
        }

    }

    fun getForecastDay(): MutableLiveData<String> {
        return forecastDay
    }

    fun getWeatherType(): MutableLiveData<String> {
        return weatherType
    }

    fun getForecastDegrees(): MutableLiveData<String> {
        return forecastDegrees
    }

    override fun loadWeather(latitude: Double, longitude: Double) {}
}