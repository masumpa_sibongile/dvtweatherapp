package za.co.sibongile.masumpa.dvtweatherapp.model

import com.google.gson.annotations.SerializedName

open class Current(
    @SerializedName("name")
    var name : String? = null,
    @SerializedName("main")
    val main: Main? =null,
    @SerializedName("weather")
    val weather: List<Weather>? = null)