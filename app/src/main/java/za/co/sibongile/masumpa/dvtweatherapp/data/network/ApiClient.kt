package za.co.sibongile.masumpa.dvtweatherapp.data.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import za.co.sibongile.masumpa.dvtweatherapp.BuildConfig

class ApiClient(private val networkConnectionInterceptor: NetworkConnectionInterceptor){

    operator fun invoke() : ApiService {
        val okHttpClient = OkHttpClient().newBuilder()
            .addInterceptor(networkConnectionInterceptor)
            .addInterceptor(loggingInterceptor())
            .build()

        return retrofit(okHttpClient)
    }

    fun retrofit(okHttpClient: OkHttpClient) : ApiService{
       return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    fun loggingInterceptor() : HttpLoggingInterceptor{
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return loggingInterceptor
    }

}