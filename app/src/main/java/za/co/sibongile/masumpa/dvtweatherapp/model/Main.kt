package za.co.sibongile.masumpa.dvtweatherapp.model

import com.google.gson.annotations.SerializedName

data class Main(
    @SerializedName("temp")
    var temp : Double,
    @SerializedName("temp_min")
    var temp_min : Double,
    @SerializedName("temp_max")
    var temp_max : Double)