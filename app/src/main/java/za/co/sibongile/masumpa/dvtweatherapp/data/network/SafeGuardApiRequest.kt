package za.co.sibongile.masumpa.dvtweatherapp.data.network

import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import za.co.sibongile.masumpa.dvtweatherapp.utils.ApiException
import za.co.sibongile.masumpa.dvtweatherapp.utils.NoInternetException

abstract class SafeGuardApiRequest {

    suspend fun <T : Any> apiRequest(call: suspend () -> Response<T>): T {
        val response = call.invoke()

        try {
            if (response.isSuccessful) {
                if (response.body() != null) {
                    return response.body()!!
                } else {
                    throw ApiException(response.raw().message)
                }
            } else {
                val error = response.errorBody()?.string()
                val message = StringBuilder()

                error?.let {
                    try {
                        message.append(JSONObject(it).getString("error"))
                    } catch (e: JSONException) {
                    }

                    message.append("\n")
                }

                message.append("Error code: ${response.code()}")
                throw ApiException(message.toString())
            }
        }catch (e: NoInternetException){
            throw e
        }
    }
}