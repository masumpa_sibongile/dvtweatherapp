package za.co.sibongile.masumpa.dvtweatherapp.data.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import za.co.sibongile.masumpa.dvtweatherapp.data.network.response.CurrentResponse
import za.co.sibongile.masumpa.dvtweatherapp.data.network.response.ForecastResponse

interface ApiService {

    @GET("weather")
    suspend fun doLoadCurrentWeather(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("APPID") appId: String
    ): Response<CurrentResponse>

    @GET("forecast")
    suspend fun doLoadForecastWeather(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("APPID") appId: String
    ): Response<ForecastResponse>

}