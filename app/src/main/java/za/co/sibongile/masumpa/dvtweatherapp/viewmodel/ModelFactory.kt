package za.co.sibongile.masumpa.dvtweatherapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import za.co.sibongile.masumpa.dvtweatherapp.data.network.repository.ForecastRepository

@Suppress("UNCHECKED_CAST")
class CurrentWeatherViewModelFactory(private val repository: ForecastRepository) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CurrentWeatherViewModel(repository) as T
    }
}

@Suppress("UNCHECKED_CAST")
class ForecastWeatherViewModelFactory(private val repository: ForecastRepository) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ForecastWeatherViewModel(repository) as T
    }
}