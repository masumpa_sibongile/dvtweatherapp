package za.co.sibongile.masumpa.dvtweatherapp.model

import com.google.gson.annotations.SerializedName

open class Forecast(
    @SerializedName("list")
    val list: List<ForecastList>? = null)

